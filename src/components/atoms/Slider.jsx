import ReactSlider from "react-slider";
import styled from "styled-components";

const StyledSlider = styled(ReactSlider)`
  width: 100%;
  height: 25px;
`;
const StyledThumb = styled.div`
  height: 24px;
  width: 24px;
  background-color: var(--green);
  border-radius: 50%;
  cursor: grab;
`;

const Thumb = (props) => <StyledThumb {...props}></StyledThumb>;

const StyledTrack = styled.div`
  height: 4px;
  top: calc(50% - 2px);
  bottom: 0;
  background: ${(props) =>
    props.index === 2
      ? "var(--beige)"
      : props.index === 1
      ? "var(--green)"
      : "#ddd"};
  border-radius: 2px;
`;

const Track = (props, state) => <StyledTrack {...props} index={state.index} />;

const Slider = (props) => {
  return (
    <StyledSlider
      className='horizontal-slider'
      thumbClassName='example-thumb'
      trackClassName='example-track'
      defaultValue={[0, 100]}
      ariaLabel={["Lower thumb", "Upper thumb"]}
      ariaValuetext={(state) => `Thumb value ${state.valueNow}`}
      pearling
      minDistance={10}
      renderTrack={Track}
      renderThumb={Thumb}
      {...props}
    />
  );
};

export default Slider;
