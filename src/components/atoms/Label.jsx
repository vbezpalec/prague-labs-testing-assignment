import styled from "styled-components";

const Label = styled.div`
  color: var(--dark-grey);
  font-size: 16px;
`;

export default Label;
