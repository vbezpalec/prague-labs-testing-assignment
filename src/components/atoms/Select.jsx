import styled, { css } from "styled-components";

const selectCss = css`
  appearance: none;
  background: transparent url("./expand_more_black_24dp.svg") top 12px right 6px
    no-repeat padding-box;
  border: 1px solid var(--beige);
  border-radius: 8px;
  font-size: 16px;
  color: var(--dark-blue);
  height: 48px;
  padding: 0 34px 0 12px;
  cursor: pointer;
`;

const Select = styled(({ options, onChange, value, className }) => {
  return (
    <select
      value={value}
      onChange={(e) => onChange(e.target.value)}
      className={className}
    >
      {options.map(({ id, label, value }) => (
        <option key={id} value={value}>
          {label}
        </option>
      ))}
    </select>
  );
})`
  ${selectCss}
`;

export default Select;
