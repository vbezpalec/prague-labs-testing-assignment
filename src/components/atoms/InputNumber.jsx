import styled from "styled-components";

const StyledInput = styled.input.attrs(() => ({
  type: "number",
}))`
  border: 1px solid var(--beige);
  border-radius: 8px;
  font-size: 16px;
  color: var(--dark-blue);
  height: 48px;
  padding: 0 34px 0 12px;
  width: 100%;
`;
const Units = styled.span`
  color: var(--dark-grey);
  position: absolute;
  top: 50%;
  right: 12px;
  transform: translateY(-50%);
`;

const InputNumber = styled(
  ({ onChange, value, min, max, units, className }) => {
    const handleChange = ({ target }) => {
      const value = Number(target.value);
      onChange(value);
    };
    return (
      <span className={className}>
        <StyledInput
          value={value}
          min={min}
          max={max}
          onChange={handleChange}
        />
        {units && <Units>{units}</Units>}
      </span>
    );
  }
)`
  position: relative;
  display: inline-block;
`;

export default InputNumber;
