import styled from "styled-components";

const Button = styled.button`
  color: var(--white);
  text-align: center;
  font: normal normal 900 16px/16px Roboto;
  background-color: var(--green);
  border-radius: 8px;
  border: none;
  padding: 14px 34px 12px;
  cursor: pointer;
  transition: all 0.3s;
  transition-property: background-color, transform;

  &:hover {
    background-color: #0f7d6f;
    transform: scale(1.1);
  }
`;

export default Button;
