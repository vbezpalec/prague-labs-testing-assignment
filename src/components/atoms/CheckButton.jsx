import styled from "styled-components";

const TextContent = styled.div`
  font-size: 12px;
  line-height: 14px;
  color: var(--dark-grey);
  margin-top: 4px;
`;

const CheckButton = styled(({ title, children, active, onClick, ...rest }) => {
  return (
    <div onClick={onClick} {...rest}>
      {title}
      <TextContent>{children}</TextContent>
    </div>
  );
})`
  border: ${({ active }) =>
    active ? "1px solid var(--green)" : "1px solid var(--beige)"};
  box-shadow: ${({ active }) => (active ? "0 0 0 1px var(--green)" : "none")};
  background-color: #fff;
  border-radius: 8px;
  padding: 12px 10px 8px;
  cursor: pointer;
  transition: all 0.3s;
  transition-property: background-color, transform, border-color, box-shadow;
  &:hover {
    background-color: #f5f5f5;
    transform: scale(1.05);
  }
`;

export default CheckButton;
