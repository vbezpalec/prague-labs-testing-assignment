export const homepage = {
  title: "Karavany - Prague Labs",
  description:
    "Exkluzivní příležitost odjet na dovolenou vašich snů už zítra! Karavan za 2300 Kč/den",
  url: "http://localhost:3000",
  imageSrc: "/logo.svg",
};
