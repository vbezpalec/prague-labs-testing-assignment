import styled from "styled-components";
import Image from "next/image";

export const MaxScreenWidth = styled.div`
  max-width: 1240px;
  margin: 0 auto;
`;

export const PageContent = styled.div`
  padding-bottom: 120px;
`

const HeaderContainer = styled.header`
  border-bottom: 1px solid var(--beige);
`;

const HeaderContent = styled(MaxScreenWidth)`
  display: flex;
  align-items: center;
  height: 80px;

  @media (max-width: 400px) {
    justify-content: center;
  }
`;

export const Header = () => {
  return (
    <HeaderContainer>
      <HeaderContent>
        <Image
          src='/logo.svg'
          width={200}
          height={35}
          alt='Prague Labs Logo'
        ></Image>
      </HeaderContent>
    </HeaderContainer>
  );
};
