import styled from "styled-components";
import Label from "../../atoms/Label";
import { MaxScreenWidth } from "../../LayoutComponents";
import PriceFilter from "../../molecules/PriceFilter";
import ReservationFilter from "../../molecules/ReservationFilter";
import TypeFilter from "../../molecules/TypeFilter";
import { filterActions } from "./constants";

export const StyledLabel = styled(Label)`
  margin-bottom: 16px;
  white-space: nowrap;
`;
const Layout = styled.div`
  @media (min-width: 400px) {
    border-bottom: 1px solid var(--beige);
  }
  ${MaxScreenWidth} {
    @media (min-width: 400px) {
      display: flex;
    }
  }
`;
const FilterTile = styled.div`
  padding: 24px 16px;
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
  &:not(:last-child) {
    border-right: 1px solid var(--beige);
  }
  @media (max-width: 400px) {
    padding-left: 16px !important;
    padding-right: 16px !important;
    border-bottom: 1px solid var(--beige);
  }
`;

const Filter = ({ dispatch, state }) => {
  return (
    <Layout>
      <MaxScreenWidth>
        <FilterTile>
          <PriceFilter
            filter={state.price}
            onChange={(payload) =>
              dispatch({ type: filterActions.SET_PRICE_FILTER, payload })
            }
          />
        </FilterTile>
        <FilterTile>
          <TypeFilter
            filter={state.type}
            onChange={(payload) =>
              dispatch({ type: filterActions.SET_TYPE_FILTER, payload })
            }
          />
        </FilterTile>
        <FilterTile>
          <ReservationFilter
            filter={state.reservation}
            onChange={(payload) =>
              dispatch({ type: filterActions.SET_RESERVATION_FILTER, payload })
            }
          />
        </FilterTile>
      </MaxScreenWidth>
    </Layout>
  );
};

export default Filter;
