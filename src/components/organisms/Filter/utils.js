import {
  assoc,
  assocPath,
  both,
  compose,
  curry,
  filter as filterFn,
  flip,
  identity,
  includes,
  isEmpty,
  prop,
  propEq,
} from "rambda";
import { DEFAULT_VISIBLE_VEHICLES_COUNT, YES } from "../../constants";

const gte = curry((num1, num2) => num1 >= num2);
const lte = curry((num1, num2) => num1 <= num2);

export const assocVisibleCount = (count) =>
  assocPath("filter.visibleCount", count);

const filterPrice = curry((range) =>
  filterFn(compose(both(lte(range[0]), gte(range[1])), prop("price")))
);

const filterType = curry((types) =>
  isEmpty(types)
    ? identity
    : filterFn(compose(flip(includes)(types), prop("vehicleType")))
);

const filterReservation = curry((instantBookable) =>
  filterFn(propEq("instantBookable", instantBookable))
);

const combinedFilters = (range, types, instantBookable) =>
  compose(
    filterPrice(range),
    filterType(types),
    filterReservation(instantBookable)
  );

export const filterState = (state) =>
  assoc(
    "filteredVehicles",
    combinedFilters(
      state.filter.price,
      state.filter.type,
      state.filter.reservation === YES,
    )(state.initialVehicles)
  )(state);

export const getNewState = (filterPath, newFilterValue) =>
  compose(
    filterState,
    assocVisibleCount(DEFAULT_VISIBLE_VEHICLES_COUNT),
    assocPath(filterPath, newFilterValue)
  );
