import { append, equals, isEmpty, reject } from "rambda";
import { filterActions } from "./constants";
import { assocVisibleCount, getNewState } from "./utils";

export function filterReducer(state, { type, payload }) {
  const { filter: filters } = state;
  switch (type) {
    case filterActions.SET_PRICE_FILTER:
      return getNewState("filter.price", payload)(state);

    case filterActions.SET_TYPE_FILTER:
      const newTypes = payload.active
        ? reject(equals(payload.type))(filters.type)
        : append(payload.type, filters.type);

      if (isEmpty(newTypes)) {
        return getNewState("filter.type", [])(state);
      }

      return getNewState("filter.type", newTypes)(state);

    case filterActions.SET_RESERVATION_FILTER:
      return getNewState("filter.reservation", payload)(state);

    case filterActions.SET_VISIBLE_COUNT:
      return assocVisibleCount(payload)(state);

    default:
      throw new Error();
  }
}
