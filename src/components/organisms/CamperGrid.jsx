import React from "react";
import styled from "styled-components";
import CamperTile from "../molecules/CamperTile";

const Layout = styled.div`
  flex-wrap: wrap;
  gap: 32px;
  max-width: 1240px;
  margin: 0 auto;
  padding: 32px 16px;

  @media (min-width: 400px) {
    padding: 32px 0;
    display: flex;
  }

  ${CamperTile} {
    margin-bottom: 32px;
    @media (min-width: 400px) {
      margin-bottom: 0;
      width: calc(calc(100% - 64px) / 3);
    }
  }
`;

const CamperGrid = ({ vehicles }) => {
  return (
    <Layout>
      {vehicles.map((vehicle, index) => (
        <CamperTile key={`${vehicle.name}${index}`} vehicle={vehicle} />
      ))}
    </Layout>
  );
};

export default CamperGrid;
