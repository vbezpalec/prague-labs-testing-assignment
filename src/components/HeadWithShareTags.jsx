import Head from "next/head";

const HeadWithShareTags = ({ children, shareData }) => (
  <Head>
    {/* Primary Meta Tags */}
    <title>{shareData.title}</title>
    <meta name='title' content={shareData.title} />
    <meta name='description' content={shareData.description} />

    {/* Open Graph / Facebook */}
    <meta property='og:type' content='website' />
    <meta property='og:url' content={shareData.url} />
    <meta property='og:title' content={shareData.title} />
    <meta property='og:description' content={shareData.description} />
    <meta property='og:image' content={shareData.imageSrc} />

    {/* Twitter */}
    <meta property='twitter:card' content='summary_large_image' />
    <meta property='twitter:url' content={shareData.url} />
    <meta property='twitter:title' content={shareData.title} />
    <meta property='twitter:description' content={shareData.description} />
    <meta property='twitter:image' content={shareData.imageSrc} />

    {children}
  </Head>
);

export default HeadWithShareTags;
