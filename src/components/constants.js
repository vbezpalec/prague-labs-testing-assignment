export const YES = "yes";
export const NO = "no";

export const vehicleTypes = {
  CAMPERVAN: "Campervan",
  INTERGRATED: "Intergrated",
  BUILTIN: "BuiltIn",
  ALCOVE: "Alcove",
};

export const vehicleLabels = {
  [vehicleTypes.CAMPERVAN]: "Campervan",
  [vehicleTypes.INTERGRATED]: "Integrál",
  [vehicleTypes.BUILTIN]: "Vestavba",
  [vehicleTypes.ALCOVE]: "Přívěs",
};

export const DEFAULT_VISIBLE_VEHICLES_COUNT = 6;
