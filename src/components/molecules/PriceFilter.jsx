import { useState } from "react";
import Slider from "../atoms/Slider";
import InputNumber from "../atoms/InputNumber";
import styled from "styled-components";
import { StyledLabel } from "../organisms/Filter/Filter";

export const MIN_PRICE = 100;
export const MAX_PRICE = 10000;

const InputsContainer = styled.div`
  display: flex;
  gap: 16px;
  margin-top: 16px;

  ${InputNumber} {
    width: 50%;
    min-width: 160px;
  }
`;

const PriceFilter = ({ onChange, filter }) => {
  const [prices, setPrices] = useState(filter);

  const handleInputChange = (index, value) => {
    const newPrices = Object.assign([], prices, { [index]: value });
    setPrices(newPrices);
    onChange(newPrices);
  };

  const handleSliderChange = (prices) => {
    setPrices(prices);
  };

  return (
    <div>
      <StyledLabel>Cena za den</StyledLabel>
      <Slider
        value={prices}
        defaultValue={filter}
        min={MIN_PRICE}
        max={MAX_PRICE}
        onChange={handleSliderChange}
        onAfterChange={(prices) => onChange(prices)}
      ></Slider>
      <InputsContainer>
        <InputNumber
          min={MIN_PRICE}
          max={MAX_PRICE}
          value={prices[0]}
          onChange={(value) => handleInputChange(0, value)}
          units='Kč'
        />
        <InputNumber
          onChange={(value) => handleInputChange(1, value)}
          min={MIN_PRICE}
          max={MAX_PRICE}
          value={prices[1]}
          units='Kč'
        />
      </InputsContainer>
    </div>
  );
};

export default PriceFilter;
