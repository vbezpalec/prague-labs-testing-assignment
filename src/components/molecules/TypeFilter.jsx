import { useMemo, useCallback } from "react";
import { includes } from "rambda";
import styled from "styled-components";
import CheckButton from "../atoms/CheckButton";
import { vehicleLabels, vehicleTypes } from "../constants";
import { StyledLabel } from "../organisms/Filter/Filter";

const Layout = styled.div`
  display: flex;
  gap: 16px;
  flex-wrap: wrap;

  @media (max-width: 400px) {
    flex-wrap: wrap;

    ${CheckButton} {
      width: calc(50% - 8px);
    }
  }
  @media (min-width: 400px) {
    ${CheckButton} {
      min-width: 100px;
      width: calc(25% - 12px);
    }
  }
`;

const getActive = (filter) => ({
  campervan: includes(vehicleTypes.CAMPERVAN)(filter),
  integrated: includes(vehicleTypes.INTERGRATED)(filter),
  builtin: includes(vehicleTypes.BUILTIN)(filter),
  alcove: includes(vehicleTypes.ALCOVE)(filter),
});

const TypeFilter = ({ onChange, filter }) => {
  const handleClick = useCallback(
    (type, active) => {
      onChange({ type, active });
    },
    [onChange]
  );

  const active = useMemo(() => getActive(filter), [filter]);

  return (
    <div>
      <StyledLabel>Typ karavanu</StyledLabel>
      <Layout>
        <CheckButton
          title={vehicleLabels[vehicleTypes.CAMPERVAN]}
          active={active.campervan}
          onClick={() => handleClick(vehicleTypes.CAMPERVAN, active.campervan)}
        >
          Obytka s rozměry osobáku, se kterou dojedete všude.
        </CheckButton>
        <CheckButton
          title={vehicleLabels[vehicleTypes.INTERGRATED]}
          active={active.integrated}
          onClick={() =>
            handleClick(vehicleTypes.INTERGRATED, active.integrated)
          }
        >
          Integrál Král mezi karavany. Luxus na kolech.
        </CheckButton>
        <CheckButton
          title={vehicleLabels[vehicleTypes.BUILTIN]}
          active={active.builtin}
          onClick={() => handleClick(vehicleTypes.BUILTIN, active.builtin)}
        >
          Vestavba Celý byt geniálně poskládaný do dodávky.
        </CheckButton>
        <CheckButton
          title={vehicleLabels[vehicleTypes.ALCOVE]}
          active={active.alcove}
          onClick={() => handleClick(vehicleTypes.ALCOVE, active.alcove)}
        >
          Tažný karavan za vaše auto. Od kapkovitých až po rodinné.
        </CheckButton>
      </Layout>
    </div>
  );
};

export default TypeFilter;
