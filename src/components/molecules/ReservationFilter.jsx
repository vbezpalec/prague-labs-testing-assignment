import Select from "../atoms/Select";
import { NO, YES } from "../constants";
import { StyledLabel } from "../organisms/Filter/Filter";
import Image from "next/image";
import styled from "styled-components";

const StyledSelect = styled(Select)`
  width: 100%;
`;
const LabelContent = styled.div`
  display: flex;
  align-items: center;
`;
const ImageWrapper = styled.div`
  width: 16px;
  height: 16px;
  margin-left: 8px;
`;

const ReservationFilter = ({ onChange, filter }) => {
  return (
    <div>
      <StyledLabel>
        <LabelContent>
          Okamžitá rezervace
          <ImageWrapper>
            <Image
              src='/fast-reservation-icon.svg'
              alt='Okamžitá rezervace'
              width={16}
              height={16}
            />
          </ImageWrapper>
        </LabelContent>
      </StyledLabel>
      <StyledSelect
        value={filter}
        onChange={onChange}
        options={[
          { id: "x", label: "Ano", value: YES },
          { id: "y", label: "Ne", value: NO },
        ]}
      />
    </div>
  );
};

export default ReservationFilter;
