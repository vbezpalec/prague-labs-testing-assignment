import React from "react";
import styled, { css } from "styled-components";
import Image from "next/image";
import Label from "../atoms/Label";
import { vehicleLabels } from "../constants";

const containerCss = css`
  border-radius: 8px;
  border: 1px solid var(--beige);
  position: relative;
`;
const Type = styled.div`
  color: var(--orange);
  font-weight: 700;
  font-size: 12px;
  text-transform: uppercase;
  margin-bottom: 2px;
`;
const Name = styled.div`
  font-size: 24px;
  font-weight: 700;
`;
const Separator = styled.hr`
  border: none;
  border-bottom: 1px solid var(--beige);
`;
const Price = styled.div`
  font-size: 16px;
  font-weight: 700;
  display: flex;
  gap: 8px;
`;
const Location = styled.div`
  font-size: 14px;
  margin-bottom: 8px;
`;
const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 16px;
`;
const Content = styled.div`
  padding: 12px 16px 16px;
`;
const Features = styled.div`
  display: flex;
  align-items: center;
  gap: 12px;
`;
const FeatureWithCount = styled.span`
  display: flex;
  align-items: center;
  gap: 4px;
`;
const ImageWrapper = styled.div`
  height: 190px;
  position: relative;
`;

const showerIcon = (
  <Image src='/shower-icon.svg' alt='Sprcha' width={20} height={20}></Image>
);
const toiletIcon = (
  <Image src='/toilet-icon.svg' alt='Toaleta' width={20} height={20}></Image>
);
const seatIcon = (
  <Image src='/seat-icon.svg' alt='Sedadlo' width={20} height={20}></Image>
);
const bedIcon = (
  <Image src='/bed-icon.svg' alt='Lůžko' width={20} height={20}></Image>
);

const CamperTile = styled(
  ({
    vehicle: {
      name,
      price,
      vehicleType,
      location,
      passengersCapacity,
      sleepCapacity,
      toilet,
      shower,
      instantBookable,
      pictures,
    } = {},
    ...rest
  }) => {
    return (
      <div {...rest}>
        <ImageWrapper>
          {pictures && (
            <Image
              src={pictures[0]}
              alt={name}
              layout='fill'
              objectFit='cover'
            />
          )}
        </ImageWrapper>
        <Content>
          <Type>{vehicleLabels[vehicleType]}</Type>
          <Name>{name}</Name>
          <Separator />
          <Location>{location}</Location>
          <Features>
            {passengersCapacity && (
              <FeatureWithCount>
                {seatIcon}
                <span>{passengersCapacity}</span>
              </FeatureWithCount>
            )}
            {sleepCapacity && (
              <FeatureWithCount>
                {bedIcon}
                <span>{sleepCapacity}</span>
              </FeatureWithCount>
            )}
            {toilet && toiletIcon}
            {shower && showerIcon}
          </Features>
          <Separator />
          <Footer>
            <Label>Cena od</Label>
            <Price>
              <span>{price} Kč/den</span>
              {instantBookable && (
                <Image
                  src='/fast-reservation-icon.svg'
                  alt='Okamžitá rezervace'
                  width={16}
                  height={16}
                />
              )}
            </Price>
          </Footer>
        </Content>
      </div>
    );
  }
)`
  ${containerCss}
`;

export default CamperTile;
