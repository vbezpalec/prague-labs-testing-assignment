# My notes

- neresil jsem accessibility
- nektere barvy maji spatny kontrast - ani AA rating
- v designu nebyl specifikovany vzhled dropdownu selectu - nechal jsem nativni select
- podpora pro ruzne prohlizece a jejich verze take nebyla zminena, nebudu resit
- testy filtrovacich funkci jsem take neresil
- nebyla urcena blizsi specifikace funkcnosti slideru (zadavani mensich values do inputu nez do druheho a opacne)
- u vetsiho projektu bych pouzil nejake utils tridy, styled system nebo obycejne theming
- pokud by bylo potreba vice breakpointu, urcite bych zvolil jinou metodu - napr. konstanty
- pro svg ikony bych pouzil treba svgr loader a importoval je jako komponenty
- nepouzil jsem zadnou UI knihovnu, sahnul jsem jen po knihovne funkci na bazi funkcionalniho pristupu - rambda - bezne pouzivam ramdu, ale ta je o neco pomalejsi
- jelikoz jsem nepouzil typescript, tak by bylo dobre pouzit alespon proptypes
- vetsina obrazku z response EP nefunguji
- bylo by dobre pohrat si s nastavenim eslintu
- dobre by take bylo pouzit nejaky loader, jak pri nacitani stranky, tak treba na obrazky
- pridat favicon