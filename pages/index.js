import { slice } from "rambda";
import { useReducer } from "react";
import styled from "styled-components";
import Button from "../src/components/atoms/Button";
import {
  DEFAULT_VISIBLE_VEHICLES_COUNT,
  NO,
} from "../src/components/constants";
import HeadWithShareTags from "../src/components/HeadWithShareTags";
import { Header, PageContent } from "../src/components/LayoutComponents";
import { MAX_PRICE, MIN_PRICE } from "../src/components/molecules/PriceFilter";
import CamperGrid from "../src/components/organisms/CamperGrid";
import { filterActions } from "../src/components/organisms/Filter/constants";
import Filter from "../src/components/organisms/Filter/Filter";
import { filterReducer } from "../src/components/organisms/Filter/reducer";
import { filterState } from "../src/components/organisms/Filter/utils";
import { homepage } from "../src/components/shareData";

const StyledButton = styled(Button)`
  display: block;
  margin: 48px auto 0;
`;

const Home = ({ data }) => {
  const [state, dispatch] = useReducer(
    filterReducer,
    {
      initialVehicles: data.items,
      filteredVehicles: data.items,
      filter: {
        price: [MIN_PRICE, MAX_PRICE],
        type: [],
        reservation: NO,
        visibleCount: DEFAULT_VISIBLE_VEHICLES_COUNT,
      },
    },
    filterState
  );

  const { visibleCount } = state.filter;
  const visibleVehicles = slice(0, visibleCount, state.filteredVehicles);
  const isLoadMoreBtnVisible =
    visibleVehicles.length !== state.filteredVehicles.length;

  return (
    <PageContent>
      <HeadWithShareTags shareData={homepage} />

      <Header />
      <Filter dispatch={dispatch} state={state.filter} />
      <CamperGrid vehicles={visibleVehicles}></CamperGrid>

      {isLoadMoreBtnVisible && (
        <StyledButton
          onClick={() =>
            dispatch({
              type: filterActions.SET_VISIBLE_COUNT,
              payload: visibleCount + DEFAULT_VISIBLE_VEHICLES_COUNT,
            })
          }
        >
          Načíst další
        </StyledButton>
      )}
    </PageContent>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`http://localhost:3000/api/data`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: { data },
  };
}

export default Home;
